//LEITURA DOS CAMPOS
//------------------
let cadastroFuncional = document.querySelector("#cadastroFuncional");
let cadastroSenha = document.querySelector("#cadastroSenha");
let cadastroEmail = document.querySelector("#cadastroEmail");
let cadastroNome = document.querySelector("#cadastroNome");

//LEITURA DOS CAMPOS
//------------------
let loginFuncional = document.querySelector("#loginFuncional");
let loginSenha = document.querySelector("#loginSenha");

var codeAutorization = "";
var nomeUsuarioLogin = "";

//funçao que será executada quando for precionado o botão Cadastrar
let botaoNovoUsuario = document.querySelector("#btnNovoUsuario");
botaoNovoUsuario.onclick = function(){
    document.getElementById("cadastro").style.display = "block";
    document.getElementById("btnNovoUsuario").style.display = "none";
    document.getElementById("btnLogar").style.display = "none";
}

//funçao que será executada quando for precionado o botão Cadastrar
let botaoCancelar = document.querySelector("#btnCancelar");
botaoCancelar.onclick = function(){
    document.getElementById("cadastro").style.display = "none";
    document.getElementById("btnNovoUsuario").style.display = "block";
    document.getElementById("btnLogar").style.display = "block";
}

//funçao que será executada quando for pressionado o botão Cancelar
let botaoCadastrar = document.querySelector("#btnCadastrar");
botaoCadastrar.onclick = cadastrarUsuario;

function cadastrarUsuario (){
    let dados = {
        funcional: cadastroFuncional.value,
        senha: cadastroSenha.value,
        email: cadastroEmail.value,
        nome: cadastroNome.value
    };

    fetch("http://localhost:8080/cadastrar/usuario", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json'
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }

        return resposta.json();
    }).then(dados => {
        console.log(dados);
    });
}

//funçao que será executada quando for pressionado o botão Cancelar
let botaoLogar = document.querySelector("#btnLogar");
botaoLogar.onclick = logarUsuario;

function logarUsuario (){
    let abrir=false;
    let dados = {
        funcional: loginFuncional.value,
        senha: loginSenha.value
    };

    fetch("http://localhost:8080/login", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json'
        }
    }).then(resposta => {
        if(resposta.status !== 200){
            let resp = document.getElementById("respostaLogar");
            resp.innerHTML = "Senha ou Usuário inválido";
            return;
        } else{
            abrir = true;
            codeAutorization = resposta.headers.get("Authorization");
            console.log ("Token " + codeAutorization);
            return resposta.json();
        }
    }).then(dados => {
        if (abrir == true){
            nomeUsuarioLogin =  dados.nome;
            console.log ("Nome do usuario " + nomeUsuarioLogin);
            localStorage.setItem("wrCodeAutorization", codeAutorization);
            localStorage.setItem("wrNomeUsuarioLogin", nomeUsuarioLogin);

            var dataAtual = new Date();
            var ano = dataAtual.getFullYear();
            var mes = dataAtual.getMonth();
            var mesExtenso = getMesExtenso(mes);
            console.log (mes);
            console.log (mesExtenso);
            mes = mes + 1;
            if (mes < 10) {
            mes = "0" + mes;
            }
            
            localStorage.setItem("wrAno", ano);
            localStorage.setItem("wrMes", mes);
            localStorage.setItem("wrMesExtenso", mesExtenso);

            window.open("calendario.html","_self");
        }
    });
}

function getMesExtenso(mes){
    var arrayMes = new Array(12);
    arrayMes[0] = "Janeiro";
    arrayMes[1] = "Fevereiro";
    arrayMes[2] = "Março";
    arrayMes[3] = "Abril";
    arrayMes[4] = "Maio";
    arrayMes[5] = "Junho";
    arrayMes[6] = "Julho";
    arrayMes[7] = "Agosto";
    arrayMes[8] = "Setembro";
    arrayMes[9] = "Outubro";
    arrayMes[10] = "Novembro";
    arrayMes[11] = "Dezembro";

    return arrayMes[mes];
}


