//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");
let atividadesDia = localStorage.getItem("wrAtividadesDia");

document.getElementById("atividadeDia").innerHTML = atividadesDia;

//Chamar API para retornar os dias
getAtividadesDia();

document.querySelector("#btnFechar").onclick = voltaCalendario;
document.querySelector("#btnExcluir").onclick = verificaChecks;


function voltaCalendario(){
    window.open("calendario.html","_self");
}

document.querySelector("#btnNovaAtividade").onclick = cadastraAtividade;

function cadastraAtividade(){
    window.open("cadativ.html","_self");
}

function getAtividadesDia(){

    let data =  atividadesDia.replace("-", "");
    data =  data.replace("-", "");

    fetch("http://localhost:8080/consultaAtividadesDia/" + data , {
        method: "GET",
        headers: {
            'Content-type': 'application/json',
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status !== 200){
            let resp = document.getElementById("atividadesDia");
            resp.innerHTML = "Erro inesperado. Resposta status: " + resposta.status;
            return;
        } else{
            return resposta.json();
        }
    }).then(dados => {
        
        console.log(dados);
        let resp = document.getElementById("atividadesDia");

        for (let i in dados){
            let atividade = `<input id="checkAtividade" type="checkbox" name="${dados[i].idAtividade}"> ${dados[i].tipoAtividade}: ${dados[i].nomeAtividade}, Detalhe: ${dados[i].detalheAtividade}, Duração: ${dados[i].horaCalculada}</p>`; 
            resp.innerHTML += atividade;
            console.log (atividade);
        };
    });
};

function verificaChecks (){
    let checksAtividades = document.querySelectorAll("#checkAtividade");
    let atividadeId = "";

    console.log(checksAtividades);

    for (let i=0; i<checksAtividades.length; i++){
        if(checksAtividades[i].checked == true){
           atividadeId = checksAtividades[i].name
           console.log(checksAtividades[i]);
           excluirAtividade(atividadeId);
        }
    }
};

function excluirAtividade (atividadeId){
    
    let validaExclusao = false;
    let dados = {
        idAtividade: atividadeId,
        horaInicio: "00:00:00",
        horaFim: "00:00:00"
    };

    console.log(dados);

    fetch("http://localhost:8080/excluirAtividade", {
        method: "POST",
        body: JSON.stringify(dados),
        headers: {
            'Content-type': 'application/json', 
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status == 403){
            //
        }
        if (resposta.status == 200){
            validaExclusao = true;
        }
        return resposta.json();

    }).then(dados => {
        console.log(dados);
        if (validaExclusao == true){
            window.open("atividadesDia.html","_self");
        }

    });
}
