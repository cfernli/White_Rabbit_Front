//traz para a pagina as informacoes de token e nome
let codeAutorization = localStorage.getItem("wrCodeAutorization");
let nomeUsuarioLogin = localStorage.getItem("wrNomeUsuarioLogin");
let ano = localStorage.getItem("wrAno");
let mes = localStorage.getItem("wrMes");
let mesExtenso = localStorage.getItem("wrMesExtenso");

let feriadosAno;
let qtdeFeriadosAno;
let atividades = "";

document.getElementById("calendarioNome").innerHTML = "Olá " + nomeUsuarioLogin;

// chama API de feriados
function getFeriadosAno(){
    
    fetch("https://api.calendario.com.br/?json=true&ano=2018&ibge=3550308&token=bW9uaWNhLm1hcnRpbmVzQHVvbC5jb20uYnImaGFzaD0yMjg4OTE1MDk=" , {
    method: "GET",
    headers: {
        'Content-type': 'application/json'
    }
}).then(resposta => {
    if(resposta.status !== 200){
        resp.innerHTML = "Erro inesperado. Resposta status: " + resposta.status;
        return;
    } else{
        return resposta.json();
    }
}).then(dados => {
    feriadosAno = dados;
    qtdeFeriadosAno = dados.length;
    getAtividadesMes(); 
})};


getFeriadosAno();
var dias = ["S1D1", "S1D2", "S1D3", "S1D4", "S1D5", "S1D6", "S1D7",
"S2D1", "S2D2", "S2D3", "S2D4", "S2D5", "S2D6", "S2D7",
"S3D1", "S3D2", "S3D3", "S3D4", "S3D5", "S3D6", "S3D7",
"S4D1", "S4D2", "S4D3", "S4D4", "S4D5", "S4D6", "S4D7",
"S5D1", "S5D2", "S5D3", "S5D4", "S5D5", "S5D6", "S5D7"];

function atividadesDia (){
    dia = dias.indexOf(this.id) + 1;
    if (dia < 10) {
        dia = "0" + dia;
    }
    atividadesDia = ano + "-" + mes + "-" + dia;
    console.log("Dia Clicado " + atividadesDia);
    localStorage.setItem("wrAtividadesDia", atividadesDia);
    window.open("atividadesDia.html","_self");
}; 

function calendarioAnterior (){
    mes--;
    if (mes > 12){
        mes = 1;
        ano++;
    }
    if (mes < 1){
        mes = 12;
        ano--;
    }
    var mesExtenso = getMesExtenso(mes - 1);
    if (mes < 10) {
        mes = "0" + mes;
    }
    localStorage.setItem("wrMes", mes);
    localStorage.setItem("wrMesExtenso", mesExtenso);
    localStorage.setItem("wrAno", ano);
    window.open("calendario.html","_self");
}; 

function calendarioProximo (){
    mes++;
    if (mes > 12){
        mes = 1;
        ano++;
    }
    if (mes < 1){
        mes = 12;
        ano--;
    }
    var mesExtenso = getMesExtenso(mes - 1);
    if (mes < 10) {
        mes = "0" + mes;
    }
    localStorage.setItem("wrMes", mes);
    localStorage.setItem("wrMesExtenso", mesExtenso);
    localStorage.setItem("wrAno", ano);
    window.open("calendario.html","_self");
}; 

function getAtividadesMes(){

    let data = ano + "-" + mes + "01";
    data =  data.replace("-", "");

    fetch("http://localhost:8080/consultaAtividadesMes/" + data , {
        method: "GET",
        headers: {
            'Content-type': 'application/json',
            'Authorization': codeAutorization
        }
    }).then(resposta => {
        if(resposta.status !== 200){
            let resp = document.getElementById("atividadesDia");
            resp.innerHTML = "Erro ao consultar atividades do Mês";
            return;
        } else{
            return resposta.json();
        }
    }).then(dados => {
        atividades = dados;
        console.log(dados);
        montaCalendario();
    });
};

function verificaHorasLancadas(dia){
    let qtdeAtividades = atividades.length;
    let totMinutos = 0;
    for (let i=0; i < qtdeAtividades; i++){
        let dataDia = atividades[i].data;
        dataDia = dataDia.toString();
        dataDia = dataDia.substr(6,2);
        if (parseInt(dataDia) == parseInt(dia)){
            console.log ("Achei Dia " + dataDia);
            let horaCalculada = atividades[i].horaCalculada; 
            let horas = horaCalculada.substr(0,2);
            let minutos = atividades[i].horaCalculada.substr(3,2);
            totMinutos += (parseInt(horas) * 60) + parseInt(minutos);
        }
    }
    console.log ("tot minutos" + totMinutos);
    if (totMinutos > 0){
        let retHoras = parseInt(totMinutos / 60);
        let retMinutos = totMinutos % 60;
        if (retHoras < 10) { retHoras = "0"+retHoras; };
        if (retMinutos < 10) { retMinutos = "0"+retMinutos; };
        return "Horas Lançadas: " + retHoras + ":" + retMinutos ;
    }
    return " ";
}

function montaCalendario(){

    let mesAnt = document.querySelector(".mesAnt");
    mesAnt.onclick = calendarioAnterior;

    let mesAtual = document.querySelector(".mes");
    mesAtual.innerHTML = mesAtual.innerHTML + "<center>" + mesExtenso + " " + ano + "<center>";

    let mesProx = document.querySelector(".mesProx");
    mesProx.onclick = calendarioProximo;
    

    for (let i=0;i <= 29;i++){
        let element = document.getElementById(dias[i]);
        element.onclick = atividadesDia;
        if(i<9){
            element.innerHTML = "0" + String(i+1);
        }
        else{
            element.innerHTML = i+1;
        }    
        
        let dataFeriado = element.innerHTML + '/' + mes + '/' + ano;
        
        for(feriado of feriadosAno){
            //console.log(feriado);
            if (feriado.date == dataFeriado & feriado.type == 'Feriado Nacional'){
                //console.log("feriado");
                element.innerHTML = element.innerHTML + " <br> " + feriado.name;
                element.style.color = "red";     
            }
        }
        let dia = i+1;
        let horasLancadas = verificaHorasLancadas(dia);
        element.innerHTML = element.innerHTML + " <br> " + " <br> " + horasLancadas ;
        element.style.font = "bold 15px arial"; 
        
    }
    //console.log(qtdeFeriadosAno);
}

function getMesExtenso(mes){
    var arrayMes = new Array(12);
    arrayMes[0] = "Janeiro";
    arrayMes[1] = "Fevereiro";
    arrayMes[2] = "Março";
    arrayMes[3] = "Abril";
    arrayMes[4] = "Maio";
    arrayMes[5] = "Junho";
    arrayMes[6] = "Julho";
    arrayMes[7] = "Agosto";
    arrayMes[8] = "Setembro";
    arrayMes[9] = "Outubro";
    arrayMes[10] = "Novembro";
    arrayMes[11] = "Dezembro";

    return arrayMes[mes];
}